﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AbstractFactory
{
    abstract class ADepartment
    {
        public abstract void Insert(Department department);
        public abstract Department GetDepartment(int id);
    }
}
