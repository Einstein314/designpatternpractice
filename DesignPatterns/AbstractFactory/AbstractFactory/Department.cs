﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AbstractFactory
{
    class Department
    {
        private int id;
        public int ID
        {
            get { return id; }
            set { id = value; }
        }

        private String dptname;
        public String DptName
        {
            get { return dptname; }
            set { dptname = value; }
        }
    }
}
