﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AbstractFactory
{
    class Program
    {
        internal IFactory IFactory
        {
            get => default(IFactory);
            set
            {
            }
        }

        internal AUser AUser
        {
            get => default(AUser);
            set
            {
            }
        }

        internal ADepartment ADepartment
        {
            get => default(ADepartment);
            set
            {
            }
        }

        internal SqlserverFactory SqlserverFactory
        {
            get => default(SqlserverFactory);
            set
            {
            }
        }

        internal AccessFactory AccessFactory
        {
            get => default(AccessFactory);
            set
            {
            }
        }

        static void Main(string[] args)
        {
            User user = new User();
            Department department = new Department();

            //IFactory factory = new SqlserverFactory();
            IFactory factory = new AccessFactory();
            AUser au = factory.createUser();

            au.Insert(user);
            au.GetUser(1);

            ADepartment adpt = factory.createDepartment();

            adpt.Insert(department);
            adpt.GetDepartment(1);

            Console.ReadKey();
        }
    }
}
