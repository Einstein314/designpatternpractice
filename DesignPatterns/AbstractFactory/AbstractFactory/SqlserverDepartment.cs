﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AbstractFactory
{
    class SqlserverDepartment : ADepartment
    {
        public override Department GetDepartment(int id)
        {
            Console.WriteLine("在SQL server中 根據ID 得到Department表的一條紀錄");
            return null;
        }

        public override void Insert(Department department)
        {
            Console.WriteLine("在SQL server中 給Department增加一條紀錄");
        }
    }
}
