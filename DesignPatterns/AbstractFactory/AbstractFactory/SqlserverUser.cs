﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AbstractFactory
{
    class SqlserverUser : AUser
    {
        public override User GetUser(int id)
        {
            Console.WriteLine("在SQL server中 根據ID 得到User表的一條紀錄");
            return null;
        }

        public override void Insert(User user)
        {
            Console.WriteLine("在SQL server中 給User增加一條紀錄");
        }
    }
}
