﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AbstractFactory
{
    class AccessFactory : IFactory
    {
        internal IFactory IFactory
        {
            get => default(IFactory);
            set
            {
            }
        }

        internal AccessDepartment AccessDepartment
        {
            get => default(AccessDepartment);
            set
            {
            }
        }

        internal AccessUser AccessUser
        {
            get => default(AccessUser);
            set
            {
            }
        }

        public ADepartment createDepartment()
        {
            return new AccessDepartment();
        }

        public AUser createUser()
        {
            return new AccessUser();
        }
    }
}
