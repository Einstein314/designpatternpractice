﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AbstractFactory
{
    class SqlserverFactory : IFactory
    {
        internal IFactory IFactory
        {
            get => default(IFactory);
            set
            {
            }
        }

        internal SqlserverUser SqlserverUser
        {
            get => default(SqlserverUser);
            set
            {
            }
        }

        internal SqlserverDepartment SqlserverDepartment
        {
            get => default(SqlserverDepartment);
            set
            {
            }
        }

        public ADepartment createDepartment()
        {
            return new SqlserverDepartment();
        }

        public AUser createUser()
        {
            return new SqlserverUser();
        }
    }
}
