﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AbstractFactory
{
    class User
    {
        private int id;
        public int ID
        {
            get { return id; }
            set { id = value; }
        }

        private String usname;
        public String UsName
        {
            get { return usname; }
            set { usname = value; }
        }
    }
}
