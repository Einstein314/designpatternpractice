﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AbstractFactory
{
    abstract class AUser
    {
        public abstract void Insert(User user);
        public abstract User GetUser(int id);
    }
}
