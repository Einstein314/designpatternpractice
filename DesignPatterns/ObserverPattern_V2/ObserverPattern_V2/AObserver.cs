﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ObserverPattern_V2
{
    abstract class AObserver
    {
        protected string name;
        protected IInformer informer;

        public AObserver(string name, IInformer informer)
        {
            this.name = name;
            this.informer = informer;
        }

        public abstract void Update();
    }
}
