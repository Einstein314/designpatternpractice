﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ObserverPattern_V2
{
    class Secretary : IInformer
    {
        private List<AObserver> observers = new List<AObserver>();
        private string action;

        public void Attach(AObserver observer)
        {
            observers.Add(observer);
        }

        public void Detach(AObserver observer)
        {
            observers.Remove(observer);
        }

        public void Notify()
        {
            foreach (AObserver ao in observers)
                ao.Update();
        }

        public string InformerState
        {
            get { return action; }
            set { action = value; }
        }
    }
}
