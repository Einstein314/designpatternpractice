﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ObserverPattern_V2
{
    class NBAObserver : AObserver
    {
        public NBAObserver(string name, IInformer informer) : base(name, informer) //呼叫父類的建構元 base格式
        {
        }
        public override void Update()
        {
            Console.WriteLine(" {0} {1} 關閉NBA直播 繼續工作!", name, informer.InformerState);
        }
    }
}
