﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ObserverPattern_V2
{
    class StockObserver : AObserver
    {
        public StockObserver(string name, IInformer informer) : base(name, informer)
        {
        }
        public override void Update()
        {
            Console.WriteLine(" {0} {1} 關閉股票行情 繼續工作!", name, informer.InformerState);
        }
    }
}
