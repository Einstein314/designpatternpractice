﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ObserverPattern_V2
{
    class Program
    {
        static void Main(string[] args)
        {
            Boss Alan = new Boss();
            Secretary Camila = new Secretary();

            StockObserver colleague1 = new StockObserver("Shawn", Alan);
            NBAObserver colleague2 = new NBAObserver("Charlie", Camila);

            Alan.Attach(colleague1);
            Camila.Attach(colleague2);

            Alan.InformerState = "我 Alan 回來了";
            Camila.InformerState = "老闆回來了";

            Alan.Notify();
            Camila.Notify();

            Console.ReadKey();
        }
    }
}
