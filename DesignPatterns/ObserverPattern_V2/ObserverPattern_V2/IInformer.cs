﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ObserverPattern_V2
{
    interface IInformer
    {
        void Attach(AObserver observer);
        void Detach(AObserver observer);
        void Notify();
        string InformerState { get; set; }
    }
}
