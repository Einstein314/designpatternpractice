﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Proxy_Pattern
{
    class Program
    {
        static void Main(string[] args)
        {
            SchoolGirl chun = new SchoolGirl();
            chun.Name = "純";

            Proxy mao = new Proxy(chun);

            mao.GiveDolls();
            mao.GiveFlowers();
            mao.GiveChocolate();

            Console.ReadKey();
        }
    }
}
