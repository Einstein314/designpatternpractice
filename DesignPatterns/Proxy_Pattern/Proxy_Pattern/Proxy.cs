﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Proxy_Pattern
{
    class Proxy:IPursue
    {
        Pursuer pursuer;
        public Proxy(SchoolGirl schoolGirl)
        {
            pursuer = new Pursuer(schoolGirl);
        }


        public void GiveDolls()
        {
            pursuer.GiveDolls();
        }
        public void GiveFlowers()
        {
            pursuer.GiveFlowers();
        }
        public void GiveChocolate()
        {
            pursuer.GiveChocolate();
        }
    }
}
