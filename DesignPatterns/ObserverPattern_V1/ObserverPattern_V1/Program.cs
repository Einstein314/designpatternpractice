﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ObserverPattern_V1
{
    class Program
    {
        static void Main(string[] args) //用戶端程式碼
        {
            //櫃姐 Camila
            Secretary Camila = new Secretary();

            //看股票同事
            StockObserver colleague1 = new StockObserver("Shawn", Camila);
            StockObserver colleague2 = new StockObserver("Justin", Camila);

            //櫃姐附加幫忙兩位同事
            Camila.Attach(colleague1);
            Camila.Attach(colleague2);

            //發現老闆回來
            Camila.SecretaryAction = "老闆回來了!";
            Camila.SecretaryAction = "董事長回來了!";

            //通知有幫忙的同事
            Camila.Notify();

            Console.ReadKey();
        }
    }
}
