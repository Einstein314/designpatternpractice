﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ObserverPattern_V1
{
    class StockObserver
    {
        private string name;
        private Secretary sec;
        public StockObserver(string name, Secretary sec)
        {
            this.name = name;
            this.sec = sec;
        }
        public void Update()
        {
            Console.WriteLine("{0} {1} 關閉股票行情 繼續工作!", sec.SecretaryAction,name);
        }
    }
}
