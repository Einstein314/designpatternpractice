﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ObserverPattern_V1
{
    class Secretary
    {
        private List<StockObserver> observers = new List<StockObserver>();
        private string action; // 內部動作欄位

        //將需要請櫃台幫忙的股市觀察者加入List
        public void Attach(StockObserver observer)
        {
            observers.Add(observer);
        }

        //通知觀察者改變行動
        public void Notify()
        {
            foreach (StockObserver o in observers)
                o.Update();
        }

        //櫃台狀態
        public string SecretaryAction  //外部動作屬性
        {
            get { return action; }
            set { action = value; }//value: 當有變數變動時 皆會存進value      
        }    

    }
}
