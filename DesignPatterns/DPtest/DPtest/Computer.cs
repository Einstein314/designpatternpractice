﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Configuration;
using System.Reflection;

namespace DPtest
{
    class Computer
    {
        private Game game;
        private static readonly string program = "DPtest";
        private static readonly string gameclass = ConfigurationManager.AppSettings["GM"];
        private static readonly string classname = program + "." + gameclass;   
        public Computer(Game game)
        {
            this.game = game;
        }

        public Game create()
        {
            game = (Game)Assembly.Load(program).CreateInstance(classname);
            return game;
        }

        public void autoplay()
        {
            game.Play();
        }
    }
}
