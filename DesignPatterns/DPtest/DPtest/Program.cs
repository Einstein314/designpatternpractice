﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Reflection;
using System.Configuration;

namespace DPtest
{
    class Program
    {
        static void Main(string[] args)
        {
             string program = "DPtest";
             string gameclass = ConfigurationManager.AppSettings["GM"];
             string classname = program + "." + gameclass;
             Game game = (Game)Assembly.Load(program).CreateInstance(classname);

             Computer computer = new Computer(game);
             computer.autoplay();
                    
             Console.ReadKey();
        }
    }
}
