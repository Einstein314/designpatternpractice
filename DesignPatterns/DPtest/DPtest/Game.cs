﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DPtest
{
    abstract class Game
    {
        public abstract void Play(); 
    }
}
