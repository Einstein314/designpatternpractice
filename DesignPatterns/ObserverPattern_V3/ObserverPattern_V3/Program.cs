﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ObserverPattern_V3
{
    class Program
    {
        static void Main(string[] args)
        {
            Boss Justin = new Boss();

            StockObserver colleague1 = new StockObserver("Charlie", Justin);
            NBAObserver colleague2 = new NBAObserver("Selena", Justin);

            Justin.Update += new EventHandler(colleague1.CloseStockMarket); //可以理解為 將一個委派的實體 委派給 Justin.Update 這個方法-->傳遞註冊方法
            Justin.Update += new EventHandler(colleague2.CloseNBAlive);

            Justin.SubjectState = "我Justin回來了 ";

            Justin.Notify();

            Console.ReadKey();
        }
    }
}
