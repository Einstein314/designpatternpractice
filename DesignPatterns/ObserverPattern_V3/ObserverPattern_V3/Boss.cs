﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ObserverPattern_V3
{
    delegate void EventHandler(); //可以理解為宣告了一個特殊的'類別'  
    class Boss : Subject
    {
    
        // public delegate void EventHandler(); 

        public event EventHandler Update; //宣告一個事件 類形為委派 委派名稱叫EvenHandler 事件名稱取為Update-->可以理解為宣告了一個'類別'的變數
        private string action;

        public void Notify()
        {
            Update();
        }

        public string SubjectState
        {
            get { return action; }
            set { action = value; }
        }
    }
}
