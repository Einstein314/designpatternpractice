﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ObserverPattern_V3
{
    class NBAObserver
    {
        private string name;
        private Subject sub;
        public NBAObserver(string name, Subject sub)
        {
            this.name = name;
            this.sub = sub;
        }

        public void CloseNBAlive() //將原本的 更新() 改為 關閉股價行情()
        {
            Console.WriteLine("{0} {1} 關閉NBA直播，繼續工作!", sub.SubjectState, name);
        }
    }
}
