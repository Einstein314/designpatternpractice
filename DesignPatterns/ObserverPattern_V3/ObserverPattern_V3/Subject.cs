﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ObserverPattern_V3
{
    interface Subject
    {
        void Notify();
        string SubjectState { get; set; }
    }
}
