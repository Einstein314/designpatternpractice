﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ObserverPattern_V3
{
    class StockObserver
    {
        private string name;
        private Subject sub;
        public StockObserver(string name, Subject sub)
        {
            this.name = name;
            this.sub = sub;
        }

        public void CloseStockMarket() //將原本的 更新() 改為 關閉股價行情()
        {
            Console.WriteLine("{0} {1} 關閉股票行情，繼續工作!", sub.SubjectState, name);
        }
    }
}
