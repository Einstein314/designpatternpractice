﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FactoryMathod
{
    class Simple_NTG_Factory
    {
        public static Nightingale createNightingale(String type)
        {
            Nightingale result = null;
            switch(type)
            {
                case "學南丁格爾的大學生":
                    result = new UnderGraduater();
                    break;
                case "社區義工":
                    result = new Volunteer();
                    break;
            }
            return result;
        }
    }
}
