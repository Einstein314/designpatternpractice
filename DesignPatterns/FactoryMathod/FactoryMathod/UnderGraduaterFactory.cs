﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FactoryMathod
{
    class UnderGraduaterFactory : IFactory
    {
        public Nightingale createNightigale()
        {
            return new UnderGraduater();
        }
    }
}
