﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FactoryMathod
{
    public class Nightingale
    {
        public void sweep()
        {
            Console.WriteLine("掃地");
        }
        public void wash()
        {
            Console.WriteLine("洗衣");
        }
        public void buyRice()
        {
            Console.WriteLine("買米");
        }
    }
}
