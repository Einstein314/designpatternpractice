﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FactoryMathod
{
    class Program
    {
        static void Main(string[] args)
        {
            //簡單工廠
            Nightingale studentA = Simple_NTG_Factory.createNightingale("學南丁格爾的大學生");
            studentA.buyRice();

            Nightingale volunteerA = Simple_NTG_Factory.createNightingale("社區義工");
            volunteerA.sweep();

            //工廠方法
            IFactory factory1 = new UnderGraduaterFactory();
            Nightingale studentB = factory1.createNightigale();
            studentB.wash();

            IFactory factory2 = new VolunteerFactory();
            Nightingale volunteerB = factory2.createNightigale();
            volunteerB.sweep();

            Console.ReadKey();
        }
    }
}
