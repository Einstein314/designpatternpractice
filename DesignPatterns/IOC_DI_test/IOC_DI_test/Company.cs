﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IOC_DI_test
{
    abstract class Company 
    {
        public abstract void invest();
    }
}
