﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IOC_DI_test
{
    class Fundation 
    {
        Company company;
        public Fundation(Company company)
        {
            this.company = company;
        }
        public void decision()
        {
            company.invest();
        }
    }
}
