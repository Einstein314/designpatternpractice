﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Configuration;
using System.Reflection;

namespace IOC_DI_test
{
    class Program 
    {
        static void Main(string[] args)
        {
            string program = "IOC_DI_test";
            string compony = ConfigurationManager.AppSettings["CPN"];
            string programclass = program + "." + compony;

           
            Fundation fundation = new Fundation((Company)Assembly.Load(program).CreateInstance(programclass));
            
            fundation.decision();

            Console.ReadKey();
        }
    }
}
